import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FriendshipRequest } from './friendship.entity';
import { Repository } from 'typeorm';
import { FriendshipRequestDto } from './dto/friendship-request.dto';
import { UsersService } from '../users/users.service';
import { PaymentService } from '../payment/payment.service';
import { ConfirmFriendshipDto } from './dto/сonfirm-friendship.dto';

@Injectable()
export class FriendshipService {
  constructor(
    @InjectRepository(FriendshipRequest)
    private readonly friendshipRequestRepository: Repository<FriendshipRequest>,
    private userService: UsersService,
    private paymentService: PaymentService,
  ) {}

  async friendshipRequest(request, dto: FriendshipRequestDto) {
    const fromUser = await this.userService.getUserById(request.user.id);
    const toUser = await this.userService.getUserById(dto.toUser);

    if (!toUser || !fromUser) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    if (fromUser.emailConfirmation === false) {
      throw new HttpException('Email not confirmed', HttpStatus.BAD_REQUEST);
    }
    if (fromUser.id === toUser.id) {
      throw new HttpException("You can't add yourself", HttpStatus.BAD_REQUEST);
    }

    const existingRequest = await this.getStatus(fromUser.id, toUser.id);
    if (existingRequest) {
      throw new HttpException(
        'Friendship request already exists',
        HttpStatus.CONFLICT,
      );
    }

    const payment = await this.paymentService.numRequestByUserId(fromUser.id);
    let numRequests = payment ? payment.numRequest : 0;
    if (numRequests <= 0) {
      throw new HttpException(
        'Not enough active requests',
        HttpStatus.BAD_REQUEST,
      );
    }

    const newRequest = new FriendshipRequest();
    newRequest.fromUser = fromUser.id;
    newRequest.toUser = toUser.id;
    newRequest.status = 'created';
    await this.friendshipRequestRepository.save(newRequest);

    numRequests -= 1;
    payment.numRequest = numRequests;
    await this.paymentService.updatePayment(payment);

    return { message: 'Friend request created' };
  }

  async confirmFriendship(request, dto: ConfirmFriendshipDto) {
    const fromUser = await this.userService.getUserById(request.user.id);
    if (fromUser.emailConfirmation === false) {
      throw new HttpException('Email not confirmed', HttpStatus.BAD_REQUEST);
    }

    const friendshipRequest = await this.getStatus(dto.toUser, fromUser.id);
    if (friendshipRequest === null) {
      throw new HttpException('Request not found', HttpStatus.BAD_REQUEST);
    }

    if (friendshipRequest.fromUser === fromUser.id) {
      throw new HttpException(
        'You cannot confirm the request because you created it',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (friendshipRequest.status === 'confirm') {
      throw new HttpException(
        'Friendship request already confirmed',
        HttpStatus.BAD_REQUEST,
      );
    } else if (friendshipRequest.status === 'reject') {
      throw new HttpException(
        'Friendship request already rejected',
        HttpStatus.BAD_REQUEST,
      );
    }

    const payment = await this.paymentService.numRequestByUserId(fromUser.id);
    let numRequests = payment ? payment.numRequest : 0;

    switch (dto.response) {
      case 'confirm':
        if (numRequests <= 0) {
          throw new HttpException(
            'Not enough active requests',
            HttpStatus.BAD_REQUEST,
          );
        }

        friendshipRequest.status = 'confirm';

        numRequests -= 1;
        payment.numRequest = numRequests;
        await this.paymentService.updatePayment(payment);
        break;
      case 'reject':
        friendshipRequest.status = 'reject';
        break;
      default:
        throw new HttpException('Invalid response', HttpStatus.BAD_REQUEST);
    }

    await this.friendshipRequestRepository.save(friendshipRequest);

    return { message: 'Friendship confirmed' };
  }

  async getStatus(fromUser: number, toUser: number) {
    return await this.friendshipRequestRepository.findOne({
      where: {
        fromUser: fromUser,
        toUser: toUser,
      },
    });
  }
}
