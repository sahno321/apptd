import { Controller, Post, UseGuards, Request, Body } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { FriendshipService } from './friendship.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { FriendshipRequestDto } from './dto/friendship-request.dto';
import { ConfirmFriendshipDto } from './dto/сonfirm-friendship.dto';
import { Profile } from '../profile/profile.entity';

@ApiTags('Friendship')
@Controller('friendship')
export class FriendshipController {
  constructor(private friendshipService: FriendshipService) {}
  @ApiOperation({
    summary: 'Adding a friend request',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/request')
  @Roles('user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async friendshipRequest(@Request() req, @Body() dto: FriendshipRequestDto) {
    return await this.friendshipService.friendshipRequest(req, dto);
  }

  @ApiOperation({
    summary: 'Conforming or rejecting a friend request',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/confirm')
  @Roles('user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async confirmFriendship(@Request() req, @Body() dto: ConfirmFriendshipDto) {
    return await this.friendshipService.confirmFriendship(req, dto);
  }
}
