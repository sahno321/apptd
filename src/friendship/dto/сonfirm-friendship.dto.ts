import { IsIn, IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ConfirmFriendshipDto {
  @ApiProperty({
    example: '1',
    description: 'Id of the user the request with which is confirmed',
  })
  @IsInt()
  toUser: number;
  @ApiProperty({
    enum: ['confirm', 'reject'],
    description: 'Response type: confirm or reject',
  })
  @IsIn(['confirm', 'reject'], { message: 'confirm or reject' })
  response: string;
}
