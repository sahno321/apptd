import { IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FriendshipRequestDto {
  @ApiProperty({
    example: '1',
    description: 'Id of the user to whom the request is sent',
  })
  @IsInt()
  toUser: number;
}
