import { Module } from '@nestjs/common';
import { FriendshipController } from './friendship.controller';
import { FriendshipService } from './friendship.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Role } from '../role/role.entity';
import { FriendshipRequest } from './friendship.entity';
import { UsersModule } from '../users/users.module';
import { AuthModule } from '../auth/auth.module';
import { PaymentService } from '../payment/payment.service';
import { Payment } from '../payment/payment.entity';

@Module({
  controllers: [FriendshipController],
  providers: [FriendshipService, PaymentService],
  imports: [
    TypeOrmModule.forFeature([User, Profile, Role, FriendshipRequest, Payment]),
    UsersModule,
    AuthModule,
  ],
  exports: [TypeOrmModule, FriendshipModule],
})
export class FriendshipModule {}
