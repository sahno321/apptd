import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class FriendshipRequest {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fromUser: number;

  @Column()
  toUser: number;

  @Column()
  status: string;
}
