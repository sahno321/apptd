import { IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class OtpPasswordDto {
  @ApiProperty({
    example: '1234',
    description: 'OTP password must be 4 digits ',
  })
  @IsNotEmpty()
  @Length(4, 4)
  otp: string;
}
