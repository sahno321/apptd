import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { OtpPasswordService } from './otp-password.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { OtpPasswordDto } from './dto/otp-password.dto';

import { Profile } from '../profile/profile.entity';
@ApiTags('OTP controller')
@Controller('otp-password')
export class OtpPasswordController {
  constructor(private otpPasswordService: OtpPasswordService) {}
  @ApiOperation({
    summary: 'Verify OTP password',
  })
  @ApiResponse({ status: 201, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/verify')
  @Roles('admin', 'candidate', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async verifyOtp(@Request() req, @Body() dto: OtpPasswordDto) {
    return this.otpPasswordService.verifyOtp(req, dto);
  }
}
