import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OtpPassword } from './otp-password.entity';
import { Repository } from 'typeorm';
import { OtpPasswordDto } from './dto/otp-password.dto';
import { CreatOtpPasswordDto } from './dto/crea-otp-password.dto';
import { UsersService } from '../users/users.service';

@Injectable()
export class OtpPasswordService {
  constructor(
    @InjectRepository(OtpPassword)
    private otpPasswordRepository: Repository<OtpPassword>,
    private userService: UsersService,
  ) {}
  async createOtpPassword(
    creatOtpPassword: CreatOtpPasswordDto,
  ): Promise<void> {
    const otpPassword = this.otpPasswordRepository.create(creatOtpPassword);
    await this.otpPasswordRepository.save(otpPassword);
  }

  async verifyOtp(request, dto: OtpPasswordDto): Promise<boolean> {
    const userId = request.user.id;
    const otpPassword = await this.otpPasswordRepository.findOne({
      where: { userId: userId },
      order: { createdAt: 'DESC' },
    });
    if (!otpPassword || otpPassword.otp !== dto.otp) {
      throw new NotFoundException('Invalid OTP');
    }

    const currentTimestamp = new Date().getTime();
    const expiresAtTimestamp = otpPassword.expiresAt.getTime();

    if (currentTimestamp > expiresAtTimestamp) {
      throw new NotFoundException('OTP expired');
    }

    await this.otpPasswordRepository.delete(otpPassword.id);
    const user = await this.userService.getUserById(userId);
    user.emailConfirmation = true;
    await this.userService.updateUser(user);

    return true;
  }

  async verifyOtpWhenRestPassword(
    userId: number,
    otp: string,
  ): Promise<boolean> {
    const otpPassword = await this.otpPasswordRepository.findOne({
      where: { userId: userId },
      order: { createdAt: 'DESC' },
    });

    if (!otpPassword || otpPassword.otp !== otp) {
      throw new NotFoundException('Invalid OTP');
    }

    const currentTimestamp = new Date().getTime();
    const expiresAtTimestamp = otpPassword.expiresAt.getTime();

    if (currentTimestamp > expiresAtTimestamp) {
      throw new NotFoundException('OTP expired');
    }

    await this.otpPasswordRepository.delete(otpPassword.id);

    return true;
  }
}
