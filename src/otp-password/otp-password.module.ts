import { Module } from '@nestjs/common';
import { OtpPasswordService } from './otp-password.service';
import { OtpPasswordController } from './otp-password.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Role } from '../role/role.entity';
import { OtpPassword } from './otp-password.entity';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';

@Module({
  providers: [OtpPasswordService],
  controllers: [OtpPasswordController],
  imports: [
    TypeOrmModule.forFeature([User, Profile, Role, OtpPassword]),
    AuthModule,
    UsersModule,
  ],
  exports: [TypeOrmModule],
})
export class OtpPasswordModule {}
