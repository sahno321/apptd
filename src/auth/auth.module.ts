import { forwardRef, Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
import * as process from 'process';

import { RoleService } from '../role/role.service';
import { OtpPasswordService } from '../otp-password/otp-password.service';

@Module({
  controllers: [AuthController],
  providers: [AuthService, RoleService, OtpPasswordService],
  imports: [
    forwardRef(() => UsersModule),
    JwtModule.register({
      secret: process.env.PRIVAT_KEY || 'SECRET',
      signOptions: {
        expiresIn: '24h',
      },
    }),
  ],
  exports: [AuthService, JwtModule, RoleService],
})
export class AuthModule {}
