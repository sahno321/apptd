import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ForgotPasswordDto {
  @ApiProperty({ example: 'user@gmail.com', description: 'Unique Email' })
  @IsEmail({}, { message: 'Incorrect email format' })
  @IsNotEmpty({ message: 'Mail required' })
  email: string;
}
