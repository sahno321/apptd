import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { MailerService } from '@nestjs-modules/mailer';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.entity';
import { RoleService } from '../role/role.service';
import { OtpPasswordService } from '../otp-password/otp-password.service';
import { ForgotPasswordDto } from './dto/forgot-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private roleService: RoleService,
    private otpPasswordService: OtpPasswordService,
    private mailerService: MailerService,
  ) {}
  async login(userDto: CreateUserDto) {
    const user = await this.validatorUser(userDto);
    return this.generateToken(user);
  }

  async registration(userDto: CreateUserDto) {
    const candidate = await this.userService.isEmailTaken(userDto.email);
    if (candidate) {
      throw new HttpException(
        'User with this email exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const hashPassword = await bcrypt.hash(userDto.password, 5);
    const user = await this.userService.createUser({
      ...userDto,
      password: hashPassword,
    });
    const otp = this.generateOtp();
    const expiresAt = this.calculateExpirationTime();

    await this.otpPasswordService.createOtpPassword({
      userId: user.id,
      otp,
      expiresAt,
    });
    const mailOptions = {
      to: userDto.email,
      subject: 'Регистрация',
      text: `Здравствуйте, ${userDto.email}! Ваш пароль: ${otp}`,
    };
    await this.mailerService.sendMail(mailOptions);

    return this.generateToken(user);
  }

  async forgotPassword(dto: ForgotPasswordDto) {
    const user = await this.userService.isEmailTaken(dto.email);
    if (!user) {
      throw new HttpException(
        'User with this email does not exist',
        HttpStatus.BAD_REQUEST,
      );
    }
    const otp = this.generateOtp();
    const expiresAt = this.calculateExpirationTime();

    await this.otpPasswordService.createOtpPassword({
      userId: user.id,
      otp,
      expiresAt,
    });
    const mailOptions = {
      to: dto.email,
      subject: 'password recovery',
      text: `Hello ${dto.email}! Your password recovery code : ${otp}`,
    };
    await this.mailerService.sendMail(mailOptions);
    return { message: 'Password reset email sent' };
  }

  async resetPassword(dto) {
    const user = await this.userService.isEmailTaken(dto.email);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    const isValidOtp = await this.otpPasswordService.verifyOtpWhenRestPassword(
      user.id,
      dto.otp,
    );
    if (!isValidOtp) {
      throw new NotFoundException('Invalid OTP code');
    }

    const hashedPassword = await bcrypt.hash(dto.newPassword, 5);

    await this.userService.updatePassword(user.id, hashedPassword);

    return { message: 'Password reset successfully' };
  }

  async generateToken(user: User) {
    const roleName = await this.roleService.getRoleById(user.roleId);
    const payload = { email: user.email, id: user.id, roles: roleName };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  private async validatorUser(userDto: CreateUserDto) {
    const user = await this.userService.isEmailTaken(userDto.email);
    if (!user) {
      throw new HttpException(
        'User with this email not exists',
        HttpStatus.BAD_REQUEST,
      );
    }
    const passwordEqual = await bcrypt.compare(userDto.password, user.password);
    if (user && passwordEqual) {
      return user;
    }
    throw new UnauthorizedException({ message: 'Incorrect email or password' });
  }

  private calculateExpirationTime(): Date {
    const expirationTime = new Date();
    expirationTime.setHours(expirationTime.getHours() + 1); // Set expiration time to 1 hour from now
    return expirationTime;
  }

  private generateOtp(): string {
    const digits = '0123456789';
    let otp = '';
    for (let i = 0; i < 4; i++) {
      const index = Math.floor(Math.random() * digits.length);
      otp += digits[index];
    }
    return otp;
  }
}
