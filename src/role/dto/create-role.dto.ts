import { IsEnum, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
enum Role {
  Admin = 'admin',
  Moderator = 'candidate',
  User = 'user',
}
export class CreateRoleDto {
  @ApiProperty({
    description: 'Role name',
    enum: Role,
    type: 'string',
    example: 'admin',
  })
  @IsNotEmpty()
  @IsEnum(Role)
  readonly role: Role;
}
