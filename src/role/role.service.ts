import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from './role.entity';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}
  async createRole(dto: CreateRoleDto) {
    const role = await this.roleRepository.create(dto);
    await this.roleRepository.save(role);
    return role;
  }

  async getRoleById(id: number) {
    return await this.roleRepository.findOne({ where: { id } });
  }

  async getRoleByName(role: string) {
    return await this.roleRepository.findOne({ where: { role } });
  }

  async deleteRoleById(id: number): Promise<string> {
    const role = await this.roleRepository.findOne({ where: { id } });
    if (!role) {
      throw new NotFoundException('Role not found');
    }

    await this.roleRepository.delete(id);

    return 'Role deleted';
  }
}
