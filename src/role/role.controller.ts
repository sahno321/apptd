import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { RoleService } from './role.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';

@ApiTags('Admin')
@Controller('role')
export class RoleController {
  constructor(private roleService: RoleService) {}
  @ApiOperation({ summary: 'Add role' })
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/add')
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async addRole(@Body() roleDto: CreateRoleDto) {
    return await this.roleService.createRole(roleDto);
  }

  @ApiOperation({ summary: 'Get role by id role' })
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Get('/:id')
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getRoleById(@Param('id') id: number) {
    return this.roleService.getRoleById(id);
  }

  @ApiOperation({ summary: 'Delete role by id role' })
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Delete('/:id')
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteRole(@Param('id') id: number): Promise<void> {
    await this.roleService.deleteRoleById(id);
  }
}
