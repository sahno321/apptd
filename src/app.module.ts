import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';
import { UsersModule } from './users/users.module';
import * as process from 'process';
import { RoleModule } from './role/role.module';
import { AuthModule } from './auth/auth.module';
import { ProfileModule } from './profile/profile.module';
import { InterestModule } from './interest/interest.module';
import { ImageModule } from './image/image.module';
import { OtpPasswordModule } from './otp-password/otp-password.module';
import { PaymentModule } from './payment/payment.module';
import { FriendshipModule } from './friendship/friendship.module';
import { ChatModule } from './chat/chat.module';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  controllers: [],
  providers: [],
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    UsersModule,
    RoleModule,
    AuthModule,
    ProfileModule,
    MulterModule.register({
      dest: './static/images',
    }),
    InterestModule,
    ImageModule,
    OtpPasswordModule,
    MailerModule.forRoot({
      transport: {
        service: 'gmail',
        auth: {
          user: process.env.DEV_EMAIL,
          pass: process.env.DEV_EMAIL_PASSWORD,
        },
      },
      defaults: {
        from: process.env.DEV_EMAIL,
      },
    }),
    PaymentModule,
    FriendshipModule,
    ChatModule,
  ],
})
export class AppModule {}
