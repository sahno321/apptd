import { define } from 'typeorm-seeding';
import { Payment } from '../../payment/payment.entity';

define(Payment, () => {
  const payment = new Payment();
  payment.numRequest = 5;
  payment.status = 'created';
  return payment;
});
