import { define } from 'typeorm-seeding';
import { Interest } from '../../interest/interest.entity';

const interestData = [
  'Sport',
  'Technology',
  'Art',
  'Travel',
  'Cooking',
  'Music',
];

let currentIndex = 0;

define(Interest, () => {
  const interest = new Interest();
  interest.name = interestData[currentIndex];

  // Increment the index and reset it to 0 when reaching the end of the array
  currentIndex = (currentIndex + 1) % interestData.length;

  return interest;
});
