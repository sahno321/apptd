import { define } from 'typeorm-seeding';
import { User } from '../../users/users.entity';
import * as bcrypt from 'bcryptjs';

define(User, (faker) => {
  const user = new User();
  user.email = faker.internet.email();
  const plainPassword = process.env.SEEDS_PASSWORD;
  const saltRounds = 5;
  user.password = bcrypt.hashSync(plainPassword, saltRounds);
  user.roleId = 3;
  user.emailConfirmation = true;
  return user;
});
