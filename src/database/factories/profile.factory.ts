import { define } from 'typeorm-seeding';
import { Profile } from '../../profile/profile.entity';

const cities = [
  'Kiev',
  'Lviv',
  'Kharkiv',
  'Odessa',
  'Dnipro',
  'Zaporizhzhia',
  'Luhansk',
  'Mariupol',
  'Vinnytsia',
  'Poltava',
  'Chernihiv',
];

define(Profile, (faker) => {
  const profile = new Profile();
  profile.name = faker.name.firstName();
  profile.dateOfBirth = faker.date
    .between('1995-01-01', '2005-12-31')
    .toISOString()
    .split('T')[0];
  profile.country = 'Ukrainian';

  profile.city = faker.random.arrayElement(cities);

  profile.gender = faker.random.arrayElement(['male', 'female']);
  profile.biography = faker.lorem.sentence();

  return profile;
});
