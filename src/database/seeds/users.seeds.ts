import { Factory, Seeder } from 'typeorm-seeding';
import { DataSource } from 'typeorm';
import { User } from '../../users/users.entity';
import { Profile } from '../../profile/profile.entity';
import { Payment } from '../../payment/payment.entity';
import { Interest } from '../../interest/interest.entity';

export default class CreateUserSeed implements Seeder {
  async run(factory: Factory, connection: DataSource): Promise<void> {
    const allInterests = await factory(Interest)().createMany(6);
    const users = await factory(User)().createMany(300);

    for (const user of users) {
      const profile = await factory(Profile)().create({ user });
      const payments = await factory(Payment)().create({ user });

      const shuffledInterests = this.shuffleArray(allInterests);
      const userInterests = shuffledInterests.slice(0, 3);

      user.profile = profile;
      user.payments = [payments];
      user.interests = userInterests;
    }

    await connection.manager.save(users);
  }

  private shuffleArray(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }
}
