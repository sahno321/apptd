import { Seeder, Factory } from 'typeorm-seeding';
import { DataSource } from 'typeorm';
import { Role } from '../../role/role.entity';

export default class CreateRolesSeed implements Seeder {
  public async run(factory: Factory, connection: DataSource): Promise<any> {
    const rolesData = [
      { role: 'admin' },
      { role: 'candidate' },
      { role: 'user' },
    ];

    await connection.getRepository(Role).save(rolesData);
  }
}
