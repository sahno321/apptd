import { Factory, Seeder } from 'typeorm-seeding';
import { DataSource } from 'typeorm';
import { User } from '../../users/users.entity';

export default class CreateAdminSeed implements Seeder {
  async run(factory: Factory, connection: DataSource): Promise<void> {
    const users = await factory(User)().create({
      email: 'admin@gmail.com',
      roleId: 1,
      emailConfirmation: true,
    });

    await connection.manager.save(users);
  }
}
