import { MigrationInterface, QueryRunner } from "typeorm";

export class Auto1689864190587 implements MigrationInterface {
    name = 'Auto1689864190587'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "friendship_request" ("id" SERIAL NOT NULL, "fromUser" integer NOT NULL, "toUser" integer NOT NULL, "status" character varying NOT NULL, CONSTRAINT "PK_edcbcad76468f3c8565a9d24612" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "interest" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_6619d627e204e0596968653011f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "role" ("id" SERIAL NOT NULL, "role" character varying(25) NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_367aad98203bd8afaed0d704093" UNIQUE ("role"), CONSTRAINT "PK_b36bcfe02fc8de3c57a8b2391c2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "otp_password" ("id" SERIAL NOT NULL, "userId" integer NOT NULL, "otp" character varying NOT NULL, "expiresAt" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "REL_2f8136caae0bfc9dd3c66b618e" UNIQUE ("userId"), CONSTRAINT "PK_0e94da7bf3f2dfc1b1a16803df6" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "payment" ("id" SERIAL NOT NULL, "numRequest" integer NOT NULL, "status" character varying NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_fcaec7df5adf9cac408c686b2ab" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "emailConfirmation" boolean NOT NULL DEFAULT false, "roleId" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "profileId" integer, CONSTRAINT "REL_9466682df91534dd95e4dbaa61" UNIQUE ("profileId"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "profile" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "dateOfBirth" character varying NOT NULL, "country" character varying NOT NULL, "city" character varying NOT NULL, "gender" character varying NOT NULL, "biography" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, CONSTRAINT "REL_a24972ebd73b106250713dcddd" UNIQUE ("userId"), CONSTRAINT "PK_3dd8bfc97e4a77c70971591bdcb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "image_entity" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "profileId" integer, CONSTRAINT "PK_fb554818daabc01db00d67aafde" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "report" ("id" SERIAL NOT NULL, "fromUser" character varying NOT NULL, "messageId" integer NOT NULL, "reasonReport" character varying NOT NULL, "comment" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "PK_99e4d0bea58cba73c57f935a546" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "message" ("id" SERIAL NOT NULL, "friendship_id" integer NOT NULL, "user_id" integer NOT NULL, "message" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "reportId" integer, CONSTRAINT "REL_ffa5ea5d588475ed3a175a3988" UNIQUE ("reportId"), CONSTRAINT "PK_ba01f0a3e0123651915008bc578" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "user_roles" ("UserId" integer NOT NULL, "roleId" integer NOT NULL, CONSTRAINT "PK_5bdd00e526e4ee3e9f7184b5961" PRIMARY KEY ("UserId", "roleId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_c69dac4a1ff60af9c27c4e7c8c" ON "user_roles" ("UserId") `);
        await queryRunner.query(`CREATE INDEX "IDX_86033897c009fcca8b6505d6be" ON "user_roles" ("roleId") `);
        await queryRunner.query(`CREATE TABLE "user_interests_interest" ("userId" integer NOT NULL, "interestId" integer NOT NULL, CONSTRAINT "PK_d01761bfe3b04d617d57c4927a3" PRIMARY KEY ("userId", "interestId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_5afca45962c04f51b2e91e0351" ON "user_interests_interest" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_d52a2a3498ea0af1471fd20025" ON "user_interests_interest" ("interestId") `);
        await queryRunner.query(`ALTER TABLE "otp_password" ADD CONSTRAINT "FK_2f8136caae0bfc9dd3c66b618e4" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "payment" ADD CONSTRAINT "FK_b046318e0b341a7f72110b75857" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user" ADD CONSTRAINT "FK_9466682df91534dd95e4dbaa616" FOREIGN KEY ("profileId") REFERENCES "profile"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "profile" ADD CONSTRAINT "FK_a24972ebd73b106250713dcddd9" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "image_entity" ADD CONSTRAINT "FK_a02312b7a59b6781c5b459321dd" FOREIGN KEY ("profileId") REFERENCES "profile"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_a22661c5b9cef58dd2a70b8e752" FOREIGN KEY ("friendship_id") REFERENCES "friendship_request"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_54ce30caeb3f33d68398ea10376" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "message" ADD CONSTRAINT "FK_ffa5ea5d588475ed3a175a39887" FOREIGN KEY ("reportId") REFERENCES "report"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_roles" ADD CONSTRAINT "FK_c69dac4a1ff60af9c27c4e7c8cb" FOREIGN KEY ("UserId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_roles" ADD CONSTRAINT "FK_86033897c009fcca8b6505d6be2" FOREIGN KEY ("roleId") REFERENCES "role"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "user_interests_interest" ADD CONSTRAINT "FK_5afca45962c04f51b2e91e03516" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "user_interests_interest" ADD CONSTRAINT "FK_d52a2a3498ea0af1471fd200251" FOREIGN KEY ("interestId") REFERENCES "interest"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user_interests_interest" DROP CONSTRAINT "FK_d52a2a3498ea0af1471fd200251"`);
        await queryRunner.query(`ALTER TABLE "user_interests_interest" DROP CONSTRAINT "FK_5afca45962c04f51b2e91e03516"`);
        await queryRunner.query(`ALTER TABLE "user_roles" DROP CONSTRAINT "FK_86033897c009fcca8b6505d6be2"`);
        await queryRunner.query(`ALTER TABLE "user_roles" DROP CONSTRAINT "FK_c69dac4a1ff60af9c27c4e7c8cb"`);
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_ffa5ea5d588475ed3a175a39887"`);
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_54ce30caeb3f33d68398ea10376"`);
        await queryRunner.query(`ALTER TABLE "message" DROP CONSTRAINT "FK_a22661c5b9cef58dd2a70b8e752"`);
        await queryRunner.query(`ALTER TABLE "image_entity" DROP CONSTRAINT "FK_a02312b7a59b6781c5b459321dd"`);
        await queryRunner.query(`ALTER TABLE "profile" DROP CONSTRAINT "FK_a24972ebd73b106250713dcddd9"`);
        await queryRunner.query(`ALTER TABLE "user" DROP CONSTRAINT "FK_9466682df91534dd95e4dbaa616"`);
        await queryRunner.query(`ALTER TABLE "payment" DROP CONSTRAINT "FK_b046318e0b341a7f72110b75857"`);
        await queryRunner.query(`ALTER TABLE "otp_password" DROP CONSTRAINT "FK_2f8136caae0bfc9dd3c66b618e4"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_d52a2a3498ea0af1471fd20025"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_5afca45962c04f51b2e91e0351"`);
        await queryRunner.query(`DROP TABLE "user_interests_interest"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_86033897c009fcca8b6505d6be"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_c69dac4a1ff60af9c27c4e7c8c"`);
        await queryRunner.query(`DROP TABLE "user_roles"`);
        await queryRunner.query(`DROP TABLE "message"`);
        await queryRunner.query(`DROP TABLE "report"`);
        await queryRunner.query(`DROP TABLE "image_entity"`);
        await queryRunner.query(`DROP TABLE "profile"`);
        await queryRunner.query(`DROP TABLE "user"`);
        await queryRunner.query(`DROP TABLE "payment"`);
        await queryRunner.query(`DROP TABLE "otp_password"`);
        await queryRunner.query(`DROP TABLE "role"`);
        await queryRunner.query(`DROP TABLE "interest"`);
        await queryRunner.query(`DROP TABLE "friendship_request"`);
    }

}
