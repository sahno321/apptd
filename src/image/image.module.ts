import { Module } from '@nestjs/common';
import { ImageController } from './image.controller';
import { ImageService } from './image.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Role } from '../role/role.entity';
import { ImageEntity } from './image.entity';
import { ProfileService } from '../profile/profile.service';
import { UsersService } from '../users/users.service';
import { RoleService } from '../role/role.service';
import { ProfileModule } from '../profile/profile.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  controllers: [ImageController],
  providers: [ImageService, ProfileService, UsersService, RoleService],
  imports: [
    TypeOrmModule.forFeature([User, Profile, Role, ImageEntity]),
    ProfileModule,
    AuthModule,
  ],
  exports: [ImageModule, TypeOrmModule],
})
export class ImageModule {}
