import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ImageEntity } from './image.entity';
import * as path from 'path';
import * as fs from 'fs';
import * as uuid from 'uuid';
import { ProfileService } from '../profile/profile.service';

@Injectable()
export class ImageService {
  constructor(
    @InjectRepository(ImageEntity)
    private readonly imageRepository: Repository<ImageEntity>,
    private profileService: ProfileService,
  ) {}
  async addImage(file: Express.Multer.File, request): Promise<HttpException> {
    const userId = request.user.id;
    const profile = await this.profileService.findProfileIdByUserId(userId);

    const imageCount = await this.imageRepository.count({
      where: { profile: { id: profile.id } },
    });

    if (imageCount >= 10) {
      throw new HttpException(
        'Maximum image limit exceeded',
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      const fileName = uuid.v4() + path.extname(file.originalname);
      const filePath = path.resolve(__dirname, '..', 'static', 'images');
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }
      fs.writeFileSync(path.join(filePath, fileName), file.buffer);

      const image = this.imageRepository.create({
        name: fileName,
        profile: profile,
      });

      await this.imageRepository.save(image);
      return new HttpException('Image add successfully', HttpStatus.CREATED);
    } catch (error) {
      throw new HttpException(
        'Failed to save image',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async deleteImage(request: any, imageId: number): Promise<HttpException> {
    const userId = request.user.id;
    const profile = await this.profileService.findProfileIdByUserId(userId);
    const images = await this.imageRepository.find({
      where: { profile: { id: profile.id } },
    });

    const imageToDelete = images.find((image) => image.id === imageId);

    if (!imageToDelete) {
      throw new HttpException('Image not found', HttpStatus.NOT_FOUND);
    }

    const imagePath = path.resolve(
      __dirname,
      '..',
      'static',
      'images',
      imageToDelete.name,
    );

    if (fs.existsSync(imagePath)) {
      fs.unlinkSync(imagePath);
    }

    await this.imageRepository.remove(imageToDelete);
    return new HttpException('Image deleted successfully', HttpStatus.OK);
  }
}
