import {
  Controller,
  Delete,
  Param,
  Post,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { ImageService } from './image.service';
import { Roles } from '../auth/role-auth.decorator';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { Profile } from '../profile/profile.entity';

@ApiTags('Image')
@Controller('image')
export class ImageController {
  constructor(private imageService: ImageService) {}
  @ApiOperation({
    summary: 'Add Image user',
  })
  @ApiResponse({ status: 201, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/add')
  @Roles('user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @UseInterceptors(FileInterceptor('image'))
  async addImage(@Request() req, @UploadedFile() image) {
    return this.imageService.addImage(image, req);
  }

  @ApiOperation({
    summary: 'Delete image by image id',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Delete('/:id')
  @Roles('user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteImage(
    @Request() request: any,
    @Param('imageId') imageId: number,
  ): Promise<{ message: string }> {
    return await this.imageService.deleteImage(request, imageId);
  }
}
