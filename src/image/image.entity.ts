import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Profile } from '../profile/profile.entity';

@Entity()
export class ImageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToOne(() => Profile, (profile) => profile.images, {
    onDelete: 'CASCADE',
  })
  profile: Profile;
}
