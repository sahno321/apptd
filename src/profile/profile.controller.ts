import {
  Body,
  Controller,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { ProfileService } from './profile.service';
import { CreateProfileDto } from './dto/create_profile.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { UpdateProfileDto } from './dto/update_profile.dto';
import { Profile } from './profile.entity';

@ApiTags('Profile')
@Controller('profile')
export class ProfileController {
  constructor(private profileService: ProfileService) {}
  @ApiOperation({
    summary: 'Add profile add return new token from role user = user',
  })
  @ApiResponse({ status: 201 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post()
  @Roles('admin', 'candidate')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async createProfile(@Request() req, @Body() dto: CreateProfileDto) {
    return await this.profileService.createProfile(dto, req);
  }

  @ApiOperation({
    summary: 'Update profile',
  })
  @ApiResponse({ status: 201, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Put('/update')
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async updateProfile(@Request() req, @Body() dto: UpdateProfileDto) {
    return await this.profileService.updateProfile(dto, req);
  }
}
