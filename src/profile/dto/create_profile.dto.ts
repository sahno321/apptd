import { IsDateString, IsIn, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateProfileDto {
  @ApiProperty({ example: 'name', description: 'name user' })
  @MaxLength(50)
  name: string;

  @ApiProperty({ example: '2000-01-01', description: 'date of birth' })
  @IsDateString()
  dateOfBirth: string;

  @ApiProperty({ example: 'Ukrainian', description: 'country' })
  @MaxLength(50)
  country: string;
  @ApiProperty({ example: 'Kiev', description: 'citi' })
  @MaxLength(50)
  city: string;

  @ApiProperty({ example: 'male', description: 'mal or female' })
  @IsIn(['male', 'female'], {
    message: 'Gender must be either "male" or "female"',
  })
  gender: string;

  @ApiProperty({
    example: 'lov cat',
    description: 'no more than 1000 characters',
  })
  @MaxLength(1000)
  biography: string;
}
