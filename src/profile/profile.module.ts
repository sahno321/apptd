import { Module } from '@nestjs/common';
import { ProfileController } from './profile.controller';
import { ProfileService } from './profile.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from './profile.entity';
import { UsersService } from '../users/users.service';
import { RoleService } from '../role/role.service';
import { Role } from '../role/role.entity';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([User, Profile, Role]), AuthModule],
  controllers: [ProfileController],
  providers: [ProfileService, UsersService, RoleService],
  exports: [TypeOrmModule, ProfileService],
})
export class ProfileModule {}
