import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { User } from '../users/users.entity';
import { ImageEntity } from '../image/image.entity';

@Entity()
export class Profile {
  @ApiProperty({ example: '1', description: 'Unique Id' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'name', description: 'name user' })
  @Column()
  name: string;
  @ApiProperty({ example: '2000-01-01', description: 'date of birth' })
  @Column()
  dateOfBirth: string;
  @ApiProperty({ example: 'Ukrainian', description: 'country' })
  @Column()
  country: string;
  @ApiProperty({ example: 'Kiev', description: 'citi' })
  @Column()
  city: string;
  @ApiProperty({ example: 'male', description: 'mal or female' })
  @Column()
  gender: string;
  @ApiProperty({
    example: 'lov cat',
    description: 'no more than 1000 characters',
  })
  @Column()
  biography: string;
  @OneToMany(() => ImageEntity, (image) => image.profile, {
    onDelete: 'CASCADE',
  })
  images: ImageEntity[];

  @Exclude()
  @OneToOne(() => User, (user) => user.profile, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
