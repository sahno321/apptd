import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Profile } from './profile.entity';
import { Repository } from 'typeorm';
import { CreateProfileDto } from './dto/create_profile.dto';
import { UsersService } from '../users/users.service';
import { RoleService } from '../role/role.service';
import { AuthService } from '../auth/auth.service';
import { UpdateProfileDto } from './dto/update_profile.dto';

@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(Profile) private profileRepository: Repository<Profile>,
    private userService: UsersService,

    private roleService: RoleService,
    private authService: AuthService,
  ) {}
  async createProfile(dto: CreateProfileDto, request) {
    const userId = request.user.id;
    const user = await this.userService.getUserById(userId);
    if (user.profile) {
      throw new HttpException('Profile already exist ', HttpStatus.BAD_REQUEST);
    }
    const profile = await this.profileRepository.create(dto);
    profile.user = user;
    await this.profileRepository.save(profile);
    user.profile = profile;
    const role = await this.roleService.getRoleByName('user');
    if (role) {
      user.roleId = role.id;
      await this.userService.updateUser(user);
    }
    const token = await this.authService.generateToken(user);

    return {
      token,
      status: HttpStatus.CREATED,
    };
  }

  async updateProfile(dto: UpdateProfileDto, request) {
    console.log(request.user);
    const userId = request.user.id;
    const user = await this.userService.getUserById(userId);
    if (!user.profile) {
      throw new HttpException('Profile not exist ', HttpStatus.BAD_REQUEST);
    }
    const profile = await this.profileRepository.findOne({
      where: { user: { id: userId } },
    });
    if (dto.name !== undefined && dto.name !== '') {
      profile.name = dto.name;
    }

    if (dto.dateOfBirth !== undefined && dto.dateOfBirth !== '') {
      profile.dateOfBirth = dto.dateOfBirth;
    }

    if (dto.city !== undefined && dto.city !== '') {
      profile.city = dto.city;
    }

    if (dto.country !== undefined && dto.country !== '') {
      profile.country = dto.country;
    }

    if (dto.biography !== undefined && dto.biography !== '') {
      profile.biography = dto.biography;
    }

    return await this.profileRepository.save(profile);
  }

  async findProfileIdByUserId(userId: number) {
    return this.profileRepository.findOne({ where: { user: { id: userId } } });
  }
}
