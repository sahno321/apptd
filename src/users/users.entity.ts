import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { Role } from '../role/role.entity';
import { Profile } from '../profile/profile.entity';
import { Interest } from '../interest/interest.entity';
import { OtpPassword } from '../otp-password/otp-password.entity';
import { ApiProperty } from '@nestjs/swagger';
import { Payment } from '../payment/payment.entity';

@Entity()
export class User {
  @ApiProperty({ example: '1', description: 'Unique Id' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'user@gmail', description: 'Unique Email' })
  @Column()
  email: string;

  @ApiProperty({ example: '1234', description: 'Password min4 max16' })
  @Column()
  password: string;

  @ApiProperty({
    example: 'true/false',
    description: 'Active default false , set true when created profile',
  })
  @Column({ default: false })
  emailConfirmation: boolean;

  @ApiProperty({ example: '1', description: 'RoleId' })
  @Column()
  roleId: number;

  @ManyToMany(() => Role, (role) => role.users)
  @JoinTable({
    name: 'user_roles',
    joinColumn: { name: 'UserId', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'roleId', referencedColumnName: 'id' },
  })
  roles: Role[];

  @ApiProperty({ example: Profile })
  @OneToOne(() => Profile, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn()
  profile: Profile;

  @ApiProperty({ example: Interest })
  @ManyToMany(() => Interest)
  @JoinTable()
  interests: Interest[];
  @OneToOne(() => OtpPassword, (otpPassword) => otpPassword.user, {
    nullable: true,
  })
  otpPassword: OtpPassword;

  @OneToMany(() => Payment, (payment) => payment.user, { onDelete: 'CASCADE' })
  payments: Payment[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
