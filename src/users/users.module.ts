import { forwardRef, Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users.entity';
import { Role } from '../role/role.entity';
import { RoleModule } from '../role/role.module';
import { AuthModule } from '../auth/auth.module';
import { Profile } from '../profile/profile.entity';
import { ProfileModule } from '../profile/profile.module';
import { Interest } from '../interest/interest.entity';
import { OtpPassword } from '../otp-password/otp-password.entity';
import { Payment } from '../payment/payment.entity';
import { FriendshipRequest } from '../friendship/friendship.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Role,
      Profile,
      Interest,
      OtpPassword,
      Payment,
      FriendshipRequest,
    ]),
    RoleModule,
    ProfileModule,
    forwardRef(() => AuthModule),
  ],
  exports: [TypeOrmModule, UsersService],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
