import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class DeleteUserDto {
  @ApiProperty({
    example: 'user@gmail.com',
    description: 'Unique email of the user we want to delete',
  })
  @IsEmail({}, { message: 'Incorrect email format' })
  @IsNotEmpty({ message: 'Mail required' })
  email: string;
}
