import { IsEnum, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export enum UserRole {
  Admin = 'admin',
  Candidate = 'candidate',
  User = 'user',
}
export class SetRoleDto {
  @ApiProperty({
    description: 'Role name',
    enum: UserRole,
    type: 'string',
    example: 'admin',
  })
  @IsNotEmpty()
  @IsEnum(UserRole)
  role: UserRole;
  @IsNotEmpty()
  @ApiProperty({ example: 1, description: 'UserId number' })
  userId: number;
}
