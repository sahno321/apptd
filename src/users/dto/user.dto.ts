import { User } from '../users.entity';

export interface GetUsersResponse {
  users: User[];
  totalCount: number;
  totalPages: number;
}
