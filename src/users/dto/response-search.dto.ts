import { ApiProperty } from '@nestjs/swagger';
import { Interest } from '../../interest/interest.entity';

export class ResponseSearchDto {
  @ApiProperty({ example: '1', description: 'Unique Id' })
  id: number;
  @ApiProperty({ example: 'users@gmail', description: 'Unique Email' })
  email: string;

  @ApiProperty()
  interest: Interest;
}
