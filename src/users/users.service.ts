import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './users.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { Not, Repository } from 'typeorm';
import { RoleService } from '../role/role.service';
import { SetRoleDto } from './dto/set-role.dto';
import { GetUsersResponse } from './dto/user.dto';

const roleName = 'candidate';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private roleService: RoleService,
  ) {}
  async createUser(dto: CreateUserDto) {
    try {
      const user = this.usersRepository.create(dto);
      const role = await this.roleService.getRoleByName(roleName);
      user.roleId = role.id;
      await this.usersRepository.save(user);
      return user;
    } catch (error) {
      throw new HttpException(
        'Failed to create user',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async getAllUsers(
    pageNumber: number,
    pageSize: number,
  ): Promise<GetUsersResponse> {
    const [users, totalCount] = await this.usersRepository.findAndCount({
      relations: ['interests'],
      select: ['id', 'email'],
      skip: (pageNumber - 1) * pageSize,
      take: pageSize,
    });

    const totalPages = Math.ceil(totalCount / pageSize);

    return { users, totalCount, totalPages };
  }

  async isEmailTaken(email: string) {
    return await this.usersRepository.findOne({
      where: { email },
      relations: ['roles'],
    });
  }

  async getUserById(id: number): Promise<User> {
    const user = await this.usersRepository.findOne({
      where: { id },
      relations: ['profile.images', 'interests'],
      select: ['id', 'email'],
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    return user;
  }
  async setRoleUser(dto: SetRoleDto) {
    const user = await this.getUserById(dto.userId);
    const role = await this.roleService.getRoleByName(dto.role);
    if (user && role) {
      user.roleId = role.id;
      await this.usersRepository.save(user);
      return HttpStatus.CREATED;
    }
  }
  async updateUser(user: User) {
    return await this.usersRepository.save(user);
  }
  async updatePassword(id: number, newPassword: string) {
    const user = await this.usersRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException('User not found');
    }

    user.password = newPassword;
    await this.usersRepository.save(user);

    return;
  }
  async deleteUser(email: string) {
    const user = await this.usersRepository.findOne({
      where: { email },
    });
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    await this.usersRepository.remove(user);
    return true;
  }

  async searchUser(
    req,
    pageNumber: number,
    pageSize: number,
  ): Promise<GetUsersResponse> {
    const currentUser = await this.usersRepository.findOne({
      where: { id: req.user.id },
      relations: ['profile.images', 'interests'],
      select: ['id', 'email'],
    });

    if (!currentUser) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const users = await this.usersRepository.find({
      where: { id: Not(currentUser.id) },
      relations: ['profile.images', 'interests'],
      select: ['id', 'email'],
    });

    const filteredUsers = users.filter((user) => {
      const commonInterests =
        user.interests && currentUser.interests
          ? user.interests.filter((interest) =>
              currentUser.interests.some(
                (userInterest) => userInterest.id === interest.id,
              ),
            )
          : [];

      return (
        commonInterests.length > 0 &&
        user.profile.city === currentUser.profile.city
      );
    });

    filteredUsers.sort((a, b) => {
      const commonInterestsA = a.interests.filter((interest) =>
        currentUser.interests.some(
          (userInterest) => userInterest.id === interest.id,
        ),
      );
      const commonInterestsB = b.interests.filter((interest) =>
        currentUser.interests.some(
          (userInterest) => userInterest.id === interest.id,
        ),
      );

      return commonInterestsB.length - commonInterestsA.length;
    });

    const isMale = currentUser.profile.gender === 'male';

    let paginatedUsers = filteredUsers;

    if (pageNumber && pageSize) {
      const startIndex = (pageNumber - 1) * pageSize;
      const endIndex = startIndex + pageSize;
      paginatedUsers = filteredUsers.slice(startIndex, endIndex);
    }

    if (isMale) {
      paginatedUsers = paginatedUsers.filter(
        (user) => user.profile.gender === 'female',
      );
    } else {
      paginatedUsers = paginatedUsers.filter(
        (user) => user.profile.gender === 'male',
      );
    }

    return {
      users: paginatedUsers,
      totalCount: filteredUsers.length,
      totalPages: Math.ceil(filteredUsers.length / pageSize),
    };
  }
}
