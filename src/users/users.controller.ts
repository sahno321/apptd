import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Request,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/role-auth.decorator';
import { RolesGuard } from '../auth/role.guard';
import { SetRoleDto } from './dto/set-role.dto';
import { User } from './users.entity';
import { DeleteUserDto } from './dto/delete-user.dto';
import { GetUsersResponse } from './dto/user.dto';
import { ResponseSearchDto } from './dto/response-search.dto';

@Controller('user')
export class UsersController {
  constructor(private userService: UsersService) {}
  @ApiTags('User')
  @ApiOperation({ summary: 'Returns the user by interests and cities' })
  @ApiResponse({ type: ResponseSearchDto })
  @ApiBearerAuth()
  @Get('/search')
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async searchUser(
    @Request() req,
    @Query('pageNumber') pageNumber: number = 1,
    @Query('pageSize') pageSize: number = 10,
  ): Promise<GetUsersResponse> {
    return await this.userService.searchUser(req, pageNumber, pageSize);
  }
  @ApiTags('User')
  @ApiOperation({ summary: 'Get all user' })
  @ApiBearerAuth()
  @ApiResponse({ type: ResponseSearchDto })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Get()
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getAll(
    @Query('pageSize') pageSize: number = 10,
    @Query('pageNumber') pageNumber: number = 1,
  ): Promise<GetUsersResponse> {
    return await this.userService.getAllUsers(pageNumber, pageSize);
  }
  @ApiTags('Admin')
  @ApiOperation({ summary: 'Get User by id' })
  @ApiResponse({ status: 200, type: User })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Get('/:id')
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getUserById(@Param('id') id: number) {
    const user = await this.userService.getUserById(id);
    return { user };
  }

  @ApiTags('Admin')
  @ApiOperation({ summary: 'Set Role' })
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @Post('/set-role')
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async setRoleUser(@Body() dto: SetRoleDto) {
    return this.userService.setRoleUser(dto);
  }
  @ApiTags('Admin')
  @ApiOperation({ summary: 'Delete User' })
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Delete()
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteUser(@Body() dto: DeleteUserDto) {
    return await this.userService.deleteUser(dto.email);
  }
}
