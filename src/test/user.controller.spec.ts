import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { User } from '../users/users.entity';

describe('UserController', () => {
  let userController: UsersController;
  let userService: UsersService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    userController = module.get<UsersController>(UsersController);
    userService = module.get<UsersService>(UsersService);
  });

  describe('getAll', () => {
    it('should return a list of users', async () => {
      const mockResult = {
        users: [],
        totalCount: 0,
        totalPages: 0,
      };
      jest.spyOn(userService, 'getAllUsers').mockResolvedValue(mockResult);

      const result = await userController.getAll(10, 1);
      expect(result);
    });
  });
  describe('getById', () => {
    it('return user by id ', async () => {
      const mockResult = { id: 4 };
      jest
        .spyOn(userService, 'getUserById')
        .mockResolvedValue(mockResult as User);
      const result = await userController.getUserById(4);
      expect(result.user.id).toEqual(mockResult.id);
    });
  });
});
