import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersController } from '../users/users.controller';
import { UsersService } from '../users/users.service';
import { RoleService } from '../role/role.service';
import { SetRoleDto, UserRole } from '../users/dto/set-role.dto';
import { User } from '../users/users.entity';

describe('UsersController', () => {
  let controller: UsersController;
  let userService: UsersService;
  let roleService: RoleService;

  beforeEach(async () => {
    const mockUserService = { getUserById: jest.fn(), setRoleUser: jest.fn() };
    const mockRoleService = {};
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: UsersService, useValue: mockUserService },
        { provide: RoleService, useValue: mockRoleService },
        JwtService,
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    userService = module.get<UsersService>(UsersService);
    roleService = module.get<RoleService>(RoleService);
  });

  describe('setRoleUser', () => {
    it('should set user role', async () => {
      const dto: SetRoleDto = {
        userId: 8,
        role: UserRole.Admin,
      };
      const userId = 8;
      const mockUser = new User();
      mockUser.id = userId;
      jest.spyOn(userService, 'getUserById').mockResolvedValue(mockUser);
      jest
        .spyOn(userService, 'setRoleUser')
        .mockResolvedValue(HttpStatus.CREATED);

      const result = await controller.setRoleUser(dto);
      expect(result).toEqual(HttpStatus.CREATED);
      expect(userService.setRoleUser).toHaveBeenCalledWith(dto);
    });
  });
});
