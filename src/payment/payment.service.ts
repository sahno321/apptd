import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Payment } from './payment.entity';
import { Repository } from 'typeorm';
import { CreatePaymentDto } from './dto/creat-payment.dto';

@Injectable()
export class PaymentService {
  constructor(
    @InjectRepository(Payment)
    private paymentRepository: Repository<Payment>,
  ) {}
  async createPayment(request, dto: CreatePaymentDto): Promise<Payment> {
    const userId = request.user.id;
    if (!dto.numRequest) {
      throw new HttpException(
        'Number of requests not specified',
        HttpStatus.BAD_REQUEST,
      );
    }
    let payment = await this.paymentRepository.findOne({ where: { userId } });
    if (!payment) {
      payment = new Payment();
      payment.numRequest = dto.numRequest;
      payment.status = 'created';
      payment.userId = userId;
    } else {
      payment.numRequest += dto.numRequest;
    }

    await this.paymentRepository.save(payment);

    return payment;
  }

  async numRequestByUserId(userId: number): Promise<Payment> {
    return await this.paymentRepository.findOne({ where: { userId } });
  }
  async updatePayment(payment: Payment): Promise<Payment> {
    return this.paymentRepository.save(payment);
  }
}
