import { IsIn, IsInt } from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class CreatePaymentDto {
  @ApiProperty({
    example: '5',
    description: 'number of requests can be 1 ,5 ,10',
  })
  @IsInt()
  @IsIn([1, 5, 10])
  @Transform(({ value }) => parseInt(value))
  numRequest: number;
}
