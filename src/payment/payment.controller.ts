import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { PaymentService } from './payment.service';
import { CreatePaymentDto } from './dto/creat-payment.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';

@ApiTags('Payment')
@Controller('payment')
export class PaymentController {
  constructor(private paymentService: PaymentService) {}
  @ApiOperation({
    summary: 'Add request',
  })
  @ApiResponse({ status: 201 })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/add')
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async creatPayment(@Request() req, @Body() dto: CreatePaymentDto) {
    return await this.paymentService.createPayment(req, dto);
  }
}
