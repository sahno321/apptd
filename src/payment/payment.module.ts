import { Module } from '@nestjs/common';
import { PaymentController } from './payment.controller';
import { PaymentService } from './payment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Role } from '../role/role.entity';
import { OtpPassword } from '../otp-password/otp-password.entity';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { Payment } from './payment.entity';

@Module({
  controllers: [PaymentController],
  providers: [PaymentService],
  imports: [
    TypeOrmModule.forFeature([User, Profile, Role, OtpPassword, Payment]),
    AuthModule,
    UsersModule,
  ],
})
export class PaymentModule {}
