import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Interest {
  @ApiProperty({ example: '1', description: 'Unique Id' })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({ example: 'Sport', description: 'Name Interest' })
  @Column()
  name: string;
}
