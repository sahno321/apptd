import {
  Controller,
  Post,
  UseGuards,
  Body,
  Request,
  Delete,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { AddInterestDto } from './dto/add-interest.dto';
import { InterestService } from './interest.service';
import { Profile } from '../profile/profile.entity';

@Controller('interest')
export class InterestController {
  constructor(private interestService: InterestService) {}
  @ApiTags('Admin')
  @ApiOperation({
    summary: 'Admin add interest ',
  })
  @ApiResponse({ status: 201, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post()
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async addInterest(@Body() dto: AddInterestDto) {
    return this.interestService.addInterest(dto);
  }

  @ApiTags('Admin')
  @ApiOperation({
    summary: 'Admin delete interest ',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Delete()
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteInterest(@Body() dto: AddInterestDto) {
    return this.interestService.deleteInterest(dto.name);
  }

  @ApiTags('Interest')
  @ApiOperation({
    summary: 'User adds their interests ',
  })
  @ApiResponse({ status: 201, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/add')
  @Roles('user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async addInterestsToUser(@Request() req, @Body() dto: AddInterestDto) {
    return this.interestService.addInterestsToUser(dto, req);
  }
}
