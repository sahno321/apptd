import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddInterestDto {
  @ApiProperty({ example: 'Sports', description: 'User interest' })
  @IsNotEmpty()
  name: string;
}
