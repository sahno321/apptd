import { Module } from '@nestjs/common';
import { InterestController } from './interest.controller';
import { InterestService } from './interest.service';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Interest } from './interest.entity';
import { UsersService } from '../users/users.service';
import { RoleService } from '../role/role.service';
import { ProfileService } from '../profile/profile.service';
import { ProfileModule } from '../profile/profile.module';

import { Role } from '../role/role.entity';
import { ImageModule } from '../image/image.module';

@Module({
  controllers: [InterestController],
  providers: [InterestService, UsersService, RoleService, ProfileService],
  imports: [
    TypeOrmModule.forFeature([User, Profile, Interest, Role]),
    AuthModule,
    ProfileModule,
    ImageModule,
  ],
  exports: [TypeOrmModule, ProfileService],
})
export class InterestModule {}
