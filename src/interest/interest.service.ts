import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Interest } from './interest.entity';
import { Repository } from 'typeorm';
import { AddInterestDto } from './dto/add-interest.dto';
import { UsersService } from '../users/users.service';

@Injectable()
export class InterestService {
  constructor(
    @InjectRepository(Interest)
    private interestRepository: Repository<Interest>,
    private userService: UsersService,
  ) {}
  async addInterest(dto: AddInterestDto) {
    await this.interestRepository.save(dto);
  }

  async findInterest(name: string): Promise<Interest> {
    return this.interestRepository.findOne({ where: { name } });
  }

  async addInterestsToUser(interests, req) {
    const userId = req.user.id;
    const user = await this.userService.getUserById(userId);
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }
    const interest = await this.interestRepository.findOne({
      where: { name: interests.name },
    });
    if (!interest) {
      throw new HttpException('Interest not found', HttpStatus.NOT_FOUND);
    }
    user.interests.push(interest);

    await this.userService.updateUser(user);
    return new HttpException('Interest add', HttpStatus.CREATED);
  }

  async deleteInterest(dto) {
    const interest = await this.interestRepository.findOne({
      where: { name: dto },
    });

    if (!interest) {
      throw new HttpException('Interest not found', HttpStatus.BAD_REQUEST);
    }
    console.log(dto.name, interest);
    await this.interestRepository.delete(interest);
    return new HttpException('Interest delete', HttpStatus.OK);
  }
}
