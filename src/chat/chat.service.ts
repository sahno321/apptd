import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from '../users/users.service';
import { FriendshipService } from '../friendship/friendship.service';
import { Repository } from 'typeorm';
import { Message } from './messege.entity';
import { CreatChatDto } from './dto/creat-chat.dto';
import { ReasonReportDto } from './dto/reason-report.dto';
import { Report } from './report.entity';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(Message)
    private readonly chatRepository: Repository<Message>,
    @InjectRepository(Report)
    private readonly reportRepository: Repository<Report>,

    private readonly userService: UsersService,
    private readonly friendshipService: FriendshipService,
  ) {}

  async startChat(req, dto: CreatChatDto) {
    const user = await this.userService.getUserById(req.user.id);
    const toUser = await this.userService.getUserById(dto.toUserId);
    const status = await this.friendshipService.getStatus(user.id, toUser.id);
    const status2 = await this.friendshipService.getStatus(toUser.id, user.id);

    if (!user || !toUser) {
      throw new HttpException('User not found', HttpStatus.BAD_REQUEST);
    } else if (
      (!status || status.status !== 'confirm') &&
      (!status2 || status2.status !== 'confirm')
    ) {
      throw new HttpException("You can't communicate", HttpStatus.FORBIDDEN);
    }

    const message = new Message();
    message.user_id = user.id;
    if (status) {
      message.friendship_id = status.id;
    } else {
      message.friendship_id = status2.id;
    }
    message.message = dto.message;
    await this.chatRepository.save(message);
    return message;
  }

  async getAllMessage(req) {
    const user = await this.userService.getUserById(req.user.id);
    return await this.chatRepository.find({
      where: { user_id: user.id },
    });
  }

  async addReport(req, dto: ReasonReportDto) {
    const user = await this.userService.getUserById(req.user.id);
    const communicate = await this.chatRepository.findOne({
      where: { user_id: user.id, id: dto.messageId },
    });

    if (!communicate) {
      throw new HttpException('Message not found', HttpStatus.NOT_FOUND);
    }
    if (
      await this.reportRepository.findOne({
        where: { messageId: dto.messageId },
      })
    ) {
      throw new HttpException('Report exist', HttpStatus.BAD_REQUEST);
    }
    const report = new Report();
    report.fromUser = user.email;
    report.messageId = dto.messageId;
    report.reasonReport = dto.reasonReport;
    report.comment = dto.comment;

    try {
      await this.reportRepository.save(report);
      return new HttpException('Report created', HttpStatus.CREATED);
    } catch (err) {
      console.log(err);
      throw new HttpException('Report not created', HttpStatus.BAD_REQUEST);
    }
  }
}
