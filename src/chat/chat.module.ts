import { Module } from '@nestjs/common';
import { ChatController } from './chat.controller';

import { ChatService } from './chat.service';
import { UsersModule } from '../users/users.module';
import { AuthModule } from '../auth/auth.module';

import { FriendshipService } from '../friendship/friendship.service';
import { PaymentService } from '../payment/payment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/users.entity';
import { Profile } from '../profile/profile.entity';
import { Role } from '../role/role.entity';
import { FriendshipRequest } from '../friendship/friendship.entity';

import { Message } from './messege.entity';
import { Report } from './report.entity';

@Module({
  controllers: [ChatController],
  providers: [ChatService, FriendshipService, PaymentService],
  imports: [
    TypeOrmModule.forFeature([
      User,
      Profile,
      Role,
      FriendshipRequest,
      Message,
      Report,
    ]),
    UsersModule,
    AuthModule,
  ],
})
export class ChatModule {}
