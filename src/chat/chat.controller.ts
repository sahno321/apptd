import {
  Controller,
  Post,
  UseGuards,
  Body,
  Request,
  Get,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Roles } from '../auth/role-auth.decorator';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role.guard';
import { ChatService } from './chat.service';
import { CreatChatDto } from './dto/creat-chat.dto';
import { ReasonReportDto } from './dto/reason-report.dto';
import { Profile } from '../profile/profile.entity';

@ApiTags('Chat')
@Controller('chat')
export class ChatController {
  constructor(private chatService: ChatService) {}
  @ApiOperation({
    summary: 'Start chat',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/start')
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async startChat(@Request() req, @Body() dto: CreatChatDto) {
    return await this.chatService.startChat(req, dto);
  }

  @ApiOperation({
    summary: 'Get all user message ',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Get()
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getAllMessage(@Request() req) {
    return await this.chatService.getAllMessage(req);
  }

  @ApiOperation({
    summary: 'Add report',
  })
  @ApiResponse({ status: 200, type: Profile })
  @ApiBearerAuth()
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiForbiddenResponse({ description: 'Forbidden' })
  @Post('/report')
  @Roles('admin', 'user')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async addReport(@Request() req, @Body() dto: ReasonReportDto) {
    return await this.chatService.addReport(req, dto);
  }
}
