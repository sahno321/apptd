import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  OneToOne,
} from 'typeorm';
import { User } from '../users/users.entity';
import { FriendshipRequest } from '../friendship/friendship.entity';
import { Report } from './report.entity';

@Entity()
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  friendship_id: number;

  @Column()
  user_id: number;

  @Column()
  message: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;

  @ManyToOne(() => FriendshipRequest)
  @JoinColumn({ name: 'friendship_id' })
  friendship: FriendshipRequest;

  @ManyToOne(() => User)
  @JoinColumn({ name: 'user_id' })
  user: User;
  @OneToOne(() => Report, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn()
  report: Report;
}
