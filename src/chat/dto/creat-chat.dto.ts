import { IsInt, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreatChatDto {
  @IsInt()
  @ApiProperty({
    example: '1',
    description: 'Id of the user to whom the begin chat',
  })
  toUserId: number;
  @ApiProperty({
    example: 'Hello!',
    description: 'Message',
  })
  @MaxLength(1000)
  message: string;
}
