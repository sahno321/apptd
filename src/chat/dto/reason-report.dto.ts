import { IsIn, IsInt, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ReasonReportDto {
  @ApiProperty({
    example: '1',
    description: 'Id of the message to whom the add report',
  })
  @IsInt()
  messageId: number;
  @ApiProperty({
    example: 'Inappropriate Content',
    description: 'Inappropriate Content, Illegal activity, Stalking, Other',
  })
  @IsIn(['Inappropriate Content', 'Illegal activity', 'Stalking', 'Other'])
  reasonReport: string;
  @ApiProperty({
    example: 'commit',
    description: 'comment with the reason for the report',
  })
  @MaxLength(1000)
  comment: string;
}
